/// <reference path='../angularjs/angular.d.ts' />
declare module BlackArrow.Services {
    interface IRemoteConfig {
        getToken(): string;
        getRootPath(): string;
        setToken(value: string): any;
        setRootPath(value: string): any;
    }
    class RemoteConfigProvider implements angular.IServiceProvider {
        private _token;
        private _rootPath;
        setToken(value: string): void;
        setRootPath(value: string): void;
        $get(): IRemoteConfig;
    }
}
declare module BlackArrow.Services {
    interface INotificationHubService {
    }
    interface ISystemAlertHubService {
    }
}
