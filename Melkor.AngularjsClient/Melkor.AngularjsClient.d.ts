/// <reference path='../angularjs/angular.d.ts' />
declare module Melkor.Models.Requests {
    interface IAllOptions {
        pageNumber?: number;
        numberPerPage?: number;
        sort?: string;
        status?: string;
        email?: string;
    }
    interface IDetailsOptions {
        developmentRequestId?: number;
        email?: string;
    }
    interface IVoteOptions {
        developmentRequestId?: number;
        email?: string;
        isUpVote?: boolean;
    }
    interface IRemoveVoteOptions {
        email?: string;
        voteId?: number;
    }
    interface ISubscribeOptions {
        developmentRequestId?: number;
        email?: string;
    }
    interface IUnsubscribeOptions {
        developmentRequestId?: number;
        subscriptionId?: number;
        email?: string;
    }
    interface IAddCommentOptions {
        developmentRequestId?: number;
        message?: string;
        email?: string;
    }
    interface ICreateOptions {
        subject?: string;
        message?: string;
        email?: string;
    }
    interface IAllTicketOptions {
        email?: string;
        userId?: number;
        organizationId?: number;
        pageNumber?: number;
        numberPerPage?: number;
        sort?: string;
    }
    interface ITicketDetailsOptions {
        email?: string;
        ticketId?: number;
    }
    interface ICreateTicketOptions {
        email?: string;
        userId?: number;
        subject?: string;
        message?: string;
        filename?: string;
        file?: string;
    }
    interface IAddCommentToTicketOptions {
        email?: string;
        ticketId?: number;
        message?: string;
        filename?: string;
        file?: string;
    }
    interface IRateTicketOptions {
        email?: string;
        ticketId?: number;
        score?: string;
        comment?: string;
    }
}
declare module Melkor.Models.Responses {
    interface IDevelopmentRequestBrief {
        id: number;
        subject: string;
        message: string;
        status: string;
        numberOfVotes: number;
        numberOfVotesUp: number;
        numberOfVotesDown: number;
        commentCount: number;
        voteId: number;
        whenUpdated: string;
        subscriptionId: number;
    }
    interface IDevelopmentRequestFull {
        id: number;
        subject: string;
        message: string;
        status: string;
        fromName: string;
        fromNursery: string;
        numberOfVotesUp: number;
        numberOfVotesDown: number;
        numberOfComments: number;
        vote: IDevelopmentRequestVote;
        subscriptionId: number;
        whenUpdated: string;
        whenCreated: string;
        comments: IDevelopmentRequestComment[];
    }
    interface IDevelopmentRequestVote {
        id: number;
        isUpVote: boolean;
    }
    interface IDevelopmentRequestComment {
        id: number;
        message: string;
        fromName: string;
        fromNursery: string;
        fromNameId: number;
        whenCommented: string;
    }
    interface ITicketBrief {
        id: number;
        subject: string;
        whenRequested: string;
        whenLastUpdated: string;
        status: string;
        assignee: IUser;
    }
    interface IUser {
        id: number;
        name: string;
        photo: IAttachment;
    }
    interface IAttachment {
        id: number;
        fileName: string;
        contentUrl: string;
        contentType: string;
        size: number;
        thumbnail: IThumbnail;
    }
    interface IThumbnail {
        id: number;
        fileName: string;
        contentUrl: string;
        contentType: string;
        size: number;
    }
    interface ITicketFull {
        id: number;
        subject: string;
        whenRequested: string;
        whenLastUpdated: string;
        status: string;
        assignee: IUser;
        ticketComments: ITicketComment[];
    }
    interface ITicketComment {
        id: number;
        body: string;
        author: IUser;
        attachments: IAttachment[];
        whenCreated: string;
    }
}
declare module Melkor.Services {
    interface IDevelopmentRequestEndpointService {
        all(model: Melkor.Models.Requests.IAllOptions): angular.IPromise<Melkor.Models.Responses.IDevelopmentRequestBrief[]>;
        details(model: Melkor.Models.Requests.IDetailsOptions): angular.IPromise<Melkor.Models.Responses.IDevelopmentRequestFull>;
        vote(model: Melkor.Models.Requests.IVoteOptions): angular.IPromise<number>;
        removeVote(model: Melkor.Models.Requests.IRemoveVoteOptions): angular.IPromise<boolean>;
        subscribe(model: Melkor.Models.Requests.ISubscribeOptions): angular.IPromise<number>;
        unsubscribe(model: Melkor.Models.Requests.IUnsubscribeOptions): angular.IPromise<boolean>;
        addComment(model: Melkor.Models.Requests.IAddCommentOptions): angular.IPromise<Melkor.Models.Responses.IDevelopmentRequestComment>;
        create(model: Melkor.Models.Requests.ICreateOptions): angular.IPromise<number>;
    }
    interface ITicketEndpointService {
        all(model: Melkor.Models.Requests.IAllTicketOptions): angular.IPromise<Melkor.Models.Responses.ITicketBrief[]>;
        details(model: Melkor.Models.Requests.ITicketDetailsOptions): angular.IPromise<Melkor.Models.Responses.ITicketFull>;
        create(model: Melkor.Models.Requests.ICreateTicketOptions): angular.IPromise<number[]>;
        addComment(model: Melkor.Models.Requests.IAddCommentToTicketOptions): angular.IPromise<number>;
        rate(model: Melkor.Models.Requests.IRateTicketOptions): angular.IPromise<number[]>;
    }
}
declare module Melkor.Services {
    interface IRemoteConfig {
        getToken(): string;
        getRootPath(): string;
        getErrorFunction(): Function;
        setToken(value: string): any;
        setRootPath(value: string): any;
        setErrorFunction(value: Function): void;
    }
    class RemoteConfigProvider implements angular.IServiceProvider {
        private _token;
        private _rootPath;
        private _errorResponse;
        setToken(value: string): void;
        setRootPath(value: string): void;
        setErrorFunction(value: Function): void;
        $get(): IRemoteConfig;
    }
}
declare module Melkor.Factories {
    function ErrorInterceptorFactory(remoteConfigProvider: Melkor.Services.IRemoteConfig, $q: angular.IQService): any;
}
